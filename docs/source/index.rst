.. dplus documentation master file, created by
   sphinx-quickstart on Tue Jun 19 16:42:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to D+ Python Interface's documentation!
=================================

dplus-api is a Python module that wraps the `D+ C++ code`_ and allows users to create and run scripts to take advantage of the D+ program's powerful x-ray diffraction simulation capabilities. 
You can get started with our readme, which contains a broad explanation of the code and many examples, or dive into our API reference.

*  :ref:`README <readme>` 
*  :ref:`API reference <dplus>`

.. _D+ C++ code: https://scholars.huji.ac.il/uriraviv/software/d-software


